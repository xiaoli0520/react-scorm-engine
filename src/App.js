import React, { useState, useEffect } from 'react';
import PropTypes from 'prop-types';


import { useSelector, useDispatch } from 'react-redux';

import { setCurrentPage } from './reducers/currentPageSlice';


import Header from './components/Header';
import Footer from './components/Footer';
import Menu from './components/Menu';
import Exit from './components/Exit';
import data from './components/data';
import Scorm from  './scorm/Scorm';

import './styles/App.scss';

import S01p01 from './components/content/s01p01';
import S01p02 from './components/content/s01p02';
import S02p01 from './components/content/s02p01';



export default function App() { 
  const currentPage = useSelector(state => state.currentPage.value);
  const pageStatus = useSelector(state => state.pageStatus);
  const dispatch = useDispatch();

  const [moduleStatus, setModuleStatus] = useState(Scorm.getLessonStatus());

  // check if module is completed
  if (Scorm.getLessonStatus() && Scorm.getLessonStatus() !== 'completed' && checkForCompletion(pageStatus)) {
    Scorm.complete();
    setModuleStatus(Scorm.getLessonStatus());
  }

  useEffect(() => {  
    // in dev mode go to page by inputting url
    if (data.devMode) {
      let urlPageId = window.location.href.split("#page=")[1];
      let pageExists = Object.keys(pageStatus).includes(urlPageId);
      if (pageExists)
        dispatch(setCurrentPage(urlPageId));
      else
        window.location.href = window.location.pathname + '#page=' + currentPage;
    }

    window.addEventListener("beforeunload", Scorm.finish);
    return () => {
      window.removeEventListener("beforeunload", Scorm.finish);
    }
  }, []);

  function checkForCompletion(status) {
    let complete = true;
    for (let page in status) {
      if (!status[page].completed)
        complete = false;
    }
    return complete;
  }

  const ExitButton = ({toggle}) => {
    return (
      <div id="exit-button" onClick={toggle}>
        <i className="fal fa-times"></i>
      </div>
    );
  }

  const switchPage = () => {
    switch(currentPage) {
      case 's01p01':
        return <S01p01 />;
      case 's01p02':
        return <S01p02 />;
      case 's02p01':
        return <S02p01 />;
      default:
        return <S01p01 />;
    }
  }

  return (
    <div className="app" id="app">
      <Header moduleName={data.name} status={moduleStatus} />    
      <Menu />
      <Exit>
        <ExitButton />
      </Exit>
      <main>
        {switchPage()}
      </main>
      <Footer />
    </div>
  )
}

App.propTypes = {
  toggle: PropTypes.func
}