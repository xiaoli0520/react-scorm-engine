let Scorm = {
  api:false,
  init() {
    this.api = window.doLMSInitialize();
    window.doLMSSetValue('cmi.core.lesson_status', 'incomplete');
  },

  setSuspendData(key, value) {
    let suspendData = window.doLMSGetValue('cmi.suspend_data');
    let data = {};
    try {
      data = suspendData? JSON.parse(suspendData) : {};
    } catch (e) {
      console.log(e);
    }
    data[key] = value;
    window.doLMSSetValue('cmi.suspend_data', JSON.stringify(data));
  },

  getSuspendData(key) {
    let suspendData = window.doLMSGetValue('cmi.suspend_data');
    let data = {};
    try {
      data = suspendData? JSON.parse(suspendData) : {};
    } catch (e) {
      console.log(e);
    }
    if (!key)
      return data;
    else {
      if (data)
        return data[key];
      else
        return null;
    }
  },

  complete() {
    window.doLMSSetValue('cmi.core.lesson_status', 'completed');
  },

  getLessonStatus() {
    return window.doLMSGetValue('cmi.core.lesson_status');
  },

  finish() {
    window.doLMSCommit();
    window.doLMSFinish();
  }
}

export default Scorm;
