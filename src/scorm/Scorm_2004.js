let Scorm = {
  api:false,
  init() {
    this.api = window.doInitialize();
    window.doSetValue('cmi.completion_status', 'incomplete');
  },

  setSuspendData(key, value) {
    let suspendData = window.doGetValue('cmi.suspend_data');
    let data = {};
    try {
      data = suspendData? JSON.parse(suspendData) : {};
    } catch (e) {
      console.log(e);
    }
    data[key] = value;
    window.doSetValue('cmi.suspend_data', JSON.stringify(data));
  },

  getSuspendData(key) {
    let suspendData = window.doGetValue('cmi.suspend_data');
    let data = {};
    try {
      data = suspendData? JSON.parse(suspendData) : {};
    } catch (e) {
      console.log(e);
    }
    if (!key)
      return data;
    else {
      if (data)
        return data[key];
      else
        return null;
    }
  },

  complete() {
    window.doSetValue('cmi.completion_status', 'completed');
  },

  getLessonStatus() {
    return window.doGetValue('cmi.completion_status');
  },

  finish() {
    window.doTerminate();
  }
}

export default Scorm;
