#!/bin/bash

rm scorm12.zip
rm scorm2004.zip

#build scorm 1.2 package
npm run build
mkdir scorm_12
cp -r ./build/* scorm_12
rm -rf scorm_12/scorm2004
rm scorm_12/SCORM_2004_APIWrapper.js
cp scorm_12/scorm12/* scorm_12
rm -rf scorm_12/scorm12
cd scorm_12
zip -r scorm12.zip .
cp scorm12.zip ../
cd ../
rm -rf scorm_12

#build scorm 2004 package
perl -pi -e 's/SCORM_12_APIWrapper.js/SCORM_2004_APIWrapper.js/' public/index.html
mv src/scorm/Scorm.js src/scorm/Scorm_temp.js 
mv src/scorm/Scorm_2004.js src/scorm/Scorm.js 
npm run build
mkdir scorm_2004
cp -r ./build/* scorm_2004
rm -rf scorm_2004/scorm12
rm scorm_2004/SCORM_12_APIWrapper.js
cp scorm_2004/scorm2004/* scorm_2004
rm -rf scorm_2004/scorm2004
cd scorm_2004
zip -r scorm2004.zip .
cp scorm2004.zip ../
cd ../
rm -rf scorm_2004
perl -pi -e 's/SCORM_2004_APIWrapper.js/SCORM_12_APIWrapper.js/' public/index.html
mv src/scorm/Scorm.js src/scorm/Scorm_2004.js 
mv src/scorm/Scorm_temp.js src/scorm/Scorm.js 