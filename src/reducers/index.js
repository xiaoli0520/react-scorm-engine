import pageStatusReducer from './pageStatusSlice';
import currentPageReducer from './currentPageSlice';

const rootReducer = { 
  pageStatus: pageStatusReducer,
  currentPage: currentPageReducer
}

export default rootReducer;