import { createSlice } from '@reduxjs/toolkit';
import data from '../components/data';
import Scorm from  '../scorm/Scorm';

let initPageStatus = Scorm.getSuspendData('pageStatus');
if (!initPageStatus) {
  initPageStatus = {};
  data.structure.forEach((section,sIndex) => {
    section.pages.forEach((page, pIndex) => {
      initPageStatus[page.id] = {id: page.id, completed: false, visited: false, disabled: sIndex === 0 && pIndex === 0? false : !data.unlockAllPages}; //always enable first page
    })
  });
}

export const pageStatusSlice = createSlice({
  name: 'page status',
  initialState: initPageStatus,
  reducers: {
    initiateStatus: (state, action) => {
      state = action.pageStatus;
    },
    visitPage: (state, action) => {
      try {
        state[action.payload].visited = true;
        state[action.payload].disabled = false;
        Scorm.setSuspendData('pageStatus', state);
      } catch(e) {
        console.log(e);
      }
    },
    completePage: (state, action) => {
      try {
        state[action.payload].completed = true;
        let pageIds = Object.keys(state);
        let nextPage = pageIds.indexOf(action.payload) < pageIds.length - 1? state[pageIds[pageIds.indexOf(action.payload) + 1]] : null;
        if (!data.customizeUnlockPage && nextPage)
          nextPage.disabled = false;
        Scorm.setSuspendData('pageStatus', state);
      } catch(e) {
        console.log(e);
      }
    },
    unlockPage: (state, action) => {
      try {
        state[action.payload].disabled = false;
        Scorm.setSuspendData('pageStatus', state);
      } catch(e) {
        console.log(e);
      }
    }
  }
});

export const { initiateStatus, visitPage, completePage } = pageStatusSlice.actions;
export default pageStatusSlice.reducer;