import { createSlice } from '@reduxjs/toolkit';

import Scorm from  '../scorm/Scorm';
import { devMode } from '../components/data.json';

const bookmark = Scorm.getSuspendData('bookmark');

export const currentPageSlice = createSlice({
  name: 'current page',
  initialState: {
    value: bookmark || 's01p01'
  },
  reducers: {
    setCurrentPage: (state, action) => {
      state.value = action.payload;
      Scorm.setSuspendData('bookmark',action.payload);
      if (devMode) 
        window.location.href = window.location.pathname + '#page=' + action.payload;
    }
  }
});

export const { setCurrentPage } = currentPageSlice.actions;

export const setCurrentPageAsync = (pageId, delay) => dispatch => {
  setTimeout(() => {
    dispatch(setCurrentPage(pageId));
  }, delay);
}; 

export default currentPageSlice.reducer;