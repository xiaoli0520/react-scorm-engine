import React from 'react';
import PropTypes from 'prop-types';

import { useSelector, useDispatch } from 'react-redux';
import { setCurrentPage } from '../reducers/currentPageSlice';


export default function Footer() {
  const currentPage = useSelector(state => state.currentPage.value);
  const pageStatus = useSelector(state => state.pageStatus);
  const dispatch = useDispatch();

  const pageIds = Object.keys(pageStatus);
  const previousPage = pageIds.indexOf(currentPage) <= 0? {disabled: true, id: currentPage} : pageStatus[pageIds[pageIds.indexOf(currentPage) - 1]];
  const nextPage = pageIds.indexOf(currentPage) < pageIds.length - 1? pageStatus[pageIds[pageIds.indexOf(currentPage) + 1]] : {disabled: true, id: currentPage};

  return (
    <footer>
      <button disabled={previousPage.disabled} onClick={() => dispatch(setCurrentPage(previousPage.id))}>Previous</button>
      <button disabled={nextPage.disabled} onClick={() => dispatch(setCurrentPage(nextPage.id))}>Next</button>
    </footer>
  )
}

Footer.propTypes = {
  navigateToPage: PropTypes.func
}