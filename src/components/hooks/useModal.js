import { useState } from 'react';

const useModal = () => {
  const [modalState, setModalState] = useState('closed');

  function toggle(speed) { // speed is in second
    if (isNaN(speed))
      speed = 500; 
    else
      speed *= 1000;  // convert to millisecond
    if (modalState === 'opened') {
      setModalState('closing');
      setTimeout(function(){
        setModalState('closed');
      }, speed);
    } else if (modalState === 'closed') {
      setModalState('opened');
    }     
  }

  return [modalState, toggle];
};

export default useModal;