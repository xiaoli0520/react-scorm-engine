import React from 'react';
import PropTypes from 'prop-types';

export default function Header({moduleName, status}) {
  return (
    <header>
      {moduleName} (status: {status})
    </header>
  )
}

Header.propTypes = {
  moduleName: PropTypes.string,
  status: PropTypes.string
}