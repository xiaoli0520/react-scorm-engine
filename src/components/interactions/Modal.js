import React, { useEffect, useState } from 'react';
import ReactDOM from 'react-dom';
import PropTypes from 'prop-types';


const Modal = ({ 
    modalState, 
    toggle, 
    noBackdrop,
    noBackdropClose, 
    header, 
    closeButton,
    hideCloseButton,
    position,
    fullWidth,
    fullHeight,
    onTop,
    children,
    classNames,
    insertTo = document.getElementById('app') }) => {
    const [modalYposition, setModalYPosition] = useState(window.innerHeight < document.body.offsetHeight? ' center-fix ' : ' ');
    function setModalVerticalLocation() {
        if (window.innerHeight < document.body.offsetHeight)
            setModalYPosition(' center-fix ');
        else
            setModalYPosition(' ');
    }

    useEffect(() => {
        setModalVerticalLocation();
        window.addEventListener('resize', setModalVerticalLocation);
    });

    let modalClass = classNames + ' modal ' + modalState + ' ' + position + (onTop? ' on-top' : '') + (fullWidth? ' full-width' : '') + (fullHeight? ' full-height ' : '');
    let modalBoxClass = 'modal-box ' + modalState + modalYposition + position + (fullWidth? ' full-width' : '') + (fullHeight? ' full-height ' : '');

    return ['opened', 'closing'].includes(modalState)? ReactDOM.createPortal(
        <div className={modalClass}>
            { !noBackdrop && <div className={`modal-backdrop ${modalState}`} onClick={()=>{return noBackdropClose? 0 : toggle();}}></div> }
            <div className={modalBoxClass}>
                { header && <div className="modal-header">
                    {header}
                </div> }
                { !hideCloseButton && <div className="modal-close" onClick={toggle}>
                    {closeButton? closeButton : 'x'}
                </div> }
                <div className="modal-body">
                    {children}
                </div>          
            </div>
        </div> 
        , insertTo
    ) : null;
}

Modal.propTypes = {
    modalState: PropTypes.oneOf(['closed', 'closing', 'opened']), 
    toggle: PropTypes.func, // function that open or close modal
    noBbackdrop: PropTypes.bool, // dark overlay
    noBackdropClose: PropTypes.bool, // close modal when clicking on overlay
    backdropToggleSpeed: PropTypes.number,
    modalToggleSpeed: PropTypes.number,
    header: PropTypes.element, 
    closeButton: PropTypes.element,
    hideCloseButton: PropTypes.bool,
    position: PropTypes.oneOf(['left', 'right', 'top', 'bottom', 'centerFix']), // modal position
    fullWidth: PropTypes.bool,
    fullHeight: PropTypes.bool,
    onTop: PropTypes.bool, //modal on top of everything, nothing else is clickable when modal is opened
    children: PropTypes.oneOfType([
        PropTypes.element,
        PropTypes.string
    ])
}

export default Modal;