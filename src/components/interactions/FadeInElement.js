import React, { useEffect, useState } from 'react';

export default function FadeInElement({children, className, delay}) {
  // speed and delay both in milli seconds
  const [showElement, setShowElement] = useState(delay? false : true);
  useEffect(() => {
    if (delay) {
      const timer = setTimeout(() => {
        setShowElement(true);
      }, delay);
      return () => clearTimeout(timer);
    }
  }, []);

  let classnames = `fadein-element ${className}${showElement && delay? ' show' : ''}`;
  if (!delay)
    classnames += ' always-show';
 
  return (
    <div className={classnames}>
      {children}
    </div>
  )
}