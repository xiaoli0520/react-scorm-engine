import React, { useEffect } from 'react';

import { useSelector, useDispatch } from 'react-redux';

import { completePage, visitPage } from '../../reducers/pageStatusSlice';

import FadeInElement from '../interactions/FadeInElement';

import styles from '../../styles/content/s01p01.module.scss';

export default function S01p01() {
  const currentPage = useSelector(state => state.currentPage.value);
  const pageStatus = useSelector(state => state.pageStatus);
  const dispatch = useDispatch();

  useEffect(() => {
    if (!pageStatus[currentPage].visited)
      dispatch(visitPage(currentPage));
  }, []);

  return (
    <div className="page">
      <p>{currentPage}</p>
      <p>visited: {pageStatus[currentPage].visited? 'Yes' : 'No'}</p>
      <p>completed: {pageStatus[currentPage].completed? 'Yes' : 'No'}</p>
      <button onClick={() => dispatch(completePage(currentPage))}>Complete page</button>
      <br/><br/><br/>
      <p>fade in element examples:</p>
      <FadeInElement className={styles['fade-in']} delay={500}>Fade in after 500ms</FadeInElement>
      <FadeInElement className={styles.fadein} delay={pageStatus[currentPage].completed? 0 : 1500} once >Fade in after 1500ms, only fade in if page is not completed</FadeInElement>
    </div>
  )
}