import React, { useEffect } from 'react';

import { useSelector, useDispatch } from 'react-redux';

import { completePage, visitPage } from '../../reducers/pageStatusSlice';

export default function S01p02() {
  const currentPage = useSelector(state => state.currentPage.value);
  const pageStatus = useSelector(state => state.pageStatus);
  const dispatch = useDispatch();

  useEffect(() => {
    if (!pageStatus[currentPage].visited)
      dispatch(visitPage(currentPage));
  }, []);

  return (
    <div className="page">
      <p>{currentPage}</p>
      <p>visited: {pageStatus[currentPage].visited? 'Yes' : 'No'}</p>
      <p>completed: {pageStatus[currentPage].completed? 'Yes' : 'No'}</p>
      <button onClick={() => dispatch(completePage(currentPage))}>Complete page</button>
    </div>
  )
}