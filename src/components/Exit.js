import React from 'react';
import Modal from './interactions/Modal';
import useModal from './hooks/useModal';
import Scorm from  '../scorm/Scorm';
import PropTypes from 'prop-types';

export default function Exit({children}) {
  const [modalState, toggle] = useModal();

  function quit() {
    Scorm.finish();
    window.parent.close();
    if (window.top !== window.self) {
      window.top.location.href = window.top.location.origin + "/#/List";
    }
  }

  return (
    <div className="exit">
      {React.cloneElement(children, { toggle: toggle })}
      <Modal 
        modalState={modalState} 
        toggle={toggle}
        noBackdropClose
        hideCloseButton
        onTop
        position="centerFix"
        classNames="exit-modal"
      >
        <div>
          <p>Are you sure you want to quit?</p>
          <button onClick={quit}>Yes</button>
          <button onClick={toggle}>No</button>
        </div>
      </Modal>    
    </div>
  );
}

Exit.propTypes = {
  children: PropTypes.element
}