import React from 'react';
import PropTypes from 'prop-types';

import { useSelector, useDispatch } from 'react-redux';
import { setCurrentPage } from '../reducers/currentPageSlice';

import { structure, showSectionInMenu } from './data.json';
import Modal from './interactions/Modal';
import useModal from './hooks/useModal';

export default function Menu() {
  const currentPage = useSelector(state => state.currentPage.value);
  const pageStatus = useSelector(state => state.pageStatus);
  const dispatch = useDispatch();

  const [modalState, toggle] = useModal();
  const menuItems = [];


  structure.forEach((section,index1) => {
    if (showSectionInMenu) {
      const sectionCompleted = checkSectionComplete(section);
      const currentSection = checkCurrentSection(section);
      menuItems.push(
        <li className={`menu-item-section${currentSection? ' current-section' : ''}${sectionCompleted? ' complete' : ''}`} key={`section-${index1}`}>{section.section}</li>
      )
    }
    section.pages.forEach((page, index2) => {
      const thisPageStatus = pageStatus[page.id];
      menuItems.push(
        <li key={`page-${index1}-${index2}`}><button className={`menu-item-page${currentPage === page.id? ' current-page' : ''}${thisPageStatus.completed? ' complete' : ''}`} onClick={() =>{clickMenuPage(page.id);}} disabled={thisPageStatus.disabled}>{page.title}</button></li>
      )
    });
  });

  function clickMenuPage(pageId) {
    toggle();
    dispatch(setCurrentPage(pageId));
  }

  function checkSectionComplete(section) {
    let complete = true;
    section.pages.forEach(page => {
      if (!pageStatus[page.id].completed)
        complete = false;
    });
    return complete;
  }

  function checkCurrentSection(section) {
    let current = false;
    section.pages.forEach(page => {
      if (page.id === currentPage)
        current = true;
    });
    return current;
  }

  return (
    <div className="main-menu">
      <button onClick={toggle}>
        main menu
      </button>
      <Modal 
        modalState={modalState} 
        toggle={toggle}
        noBackdrop
        noBackdropClose
        hideCloseButton
        position="right"
        fullHeight
        onTop
        classNames="main-menu-modal"
      >
        <nav>
          <div className="main-menu-close"><div className="close-button" onClick={toggle}><i className="fal fa-times"></i></div></div>
          <ul>
            {menuItems}
          </ul> 
        </nav>
      </Modal>    
    </div>
  )
}

Menu.propTypes = {
  navigateToPage: PropTypes.func
}